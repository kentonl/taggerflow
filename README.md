# TaggerFlow

LSTM supertagger implemented in TensorFlow.

## Dependencies
* tensorflow

## Run Experiment
`python taggerflow.py grid.json`
